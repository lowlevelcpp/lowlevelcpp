namespace student
{
void print(int        &);
void print(int       &&);
void print(int const  &);
void print(int const &&);

#include "../solution/forward_print.h"
}

#include <iostream>

namespace student
{
void print(int        &) {std::cout << "int        &" << std::endl;}
void print(int       &&) {std::cout << "int       &&" << std::endl;}
void print(int const  &) {std::cout << "int const  &" << std::endl;}
void print(int const &&) {std::cout << "int const &&" << std::endl;}
}

int main()
{
    int i = 0;
    student::forward_print(i);
    student::forward_print(0);
    student::forward_print(static_cast<int const  &>(i));
    student::forward_print(static_cast<int const &&>(i));
}
