void forward_print(int        &i) {print(i);}
void forward_print(int       &&i) {print(static_cast<int       &&>(i));}
void forward_print(int const  &i) {print(i);}
void forward_print(int const &&i) {print(static_cast<int const &&>(i));}
