#pragma once
#include "mesh.h"
namespace mesh
{

struct Cylinder : public mesh::Interface
{
    u32 dimR = 0u;
    u32 dimA = 0u;
    u32 dimZ = 0u;

    Cylinder() = default;
    Cylinder(u32 r, u32 a, u32 z)
        : mesh::Interface()
        , dimR(r)
        , dimA(a)
        , dimZ(z)
    {}

    vec3 point(u32 const cellI, u32 const faceI, u32 const pointI) const noexcept override
    {
        u32 const iz = cellI / (dimR * dimA);
        u32 const iy = (cellI - iz * dimZ) / dimR % dimA;
        u32 const ix = (cellI - iz * dimZ) % dimR;

        u32 const f[6][4] =
        {
            {0, 1, 3, 2},
            {4, 5, 7, 6},
            {0, 1, 5, 4},
            {2, 3, 7, 6},
            {0, 2, 6, 4},
            {1, 3, 7, 5},
        };
        u32 const i = f[faceI][pointI];

        double const x = static_cast<double>(ix +  (i % 2      != 0 ? 1 : 0));
        double const y = static_cast<double>(iy + ((i / 2) % 2 != 0 ? 1 : 0));
        double const z = static_cast<double>(iz +  (i / 4      != 0 ? 1 : 0));

        double const r = 0.8 * x / dimR + 0.2;
        double const phi = y == dimA
            ? 0.
            : (2. * M_PI) * (y / dimA);

        return vec3
        {
            r * std::cos(phi),
            r * std::sin(phi),
            z / dimZ,
        };
    }
    u32 pointCount(u32 const, u32 const) const noexcept override {return 4u;}
    u32 faceCount(u32 const) const noexcept override {return 6u;}
    u32 cellCount() const noexcept override {return dimR * dimA * dimZ;}
};

} // namespace mesh
