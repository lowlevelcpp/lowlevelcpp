struct CubeMesh : public mesh::Interface
{
    vec3 point(u32 const cellI, u32 const faceI, u32 const pointI) const noexcept override
    {
        u32 const iz = cellI / 256;
        u32 const iy = (cellI - iz * 16) / 16 % 16;
        u32 const ix = (cellI - iz * 16) % 16;

        u32 const f[6][4] =
        {
            {0, 1, 3, 2},
            {4, 5, 7, 6},
            {0, 1, 5, 4},
            {2, 3, 7, 6},
            {0, 2, 6, 4},
            {1, 3, 7, 5},
        };
        u32 const i = f[faceI][pointI];

        double const x = static_cast<double>(ix +  (i % 2      != 0 ? 1 : 0));
        double const y = static_cast<double>(iy + ((i / 2) % 2 != 0 ? 1 : 0));
        double const z = static_cast<double>(iz +  (i / 4      != 0 ? 1 : 0));

        return vec3
        {
            x,
            y,
            z,
        };
    }
    u32 pointCount(u32 const, u32 const) const noexcept override {return 4u;}
    u32 faceCount(u32 const) const noexcept override {return 6u;}
    u32 cellCount() const noexcept override {return 4096u;}
};
