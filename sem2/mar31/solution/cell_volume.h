vec3 faceCenter(mesh::Interface const &mesh, u32 const cellI, u32 const faceI) noexcept
{
    vec3 v = {0., 0., 0.};
    u32 const N = mesh.pointCount(cellI, faceI);
    for(u32 pointI = 0u; pointI < N; ++pointI)
        v = v + mesh.point(cellI, faceI, pointI);
    return v / double(N);
}
vec3 cellCenter(mesh::Interface const &mesh, u32 const cellI) noexcept
{
    vec3 v = {0., 0., 0.};
    u32 const N = mesh.faceCount(cellI);
    for(u32 faceI = 0u; faceI < N; ++faceI)
        v = v + faceCenter(mesh, cellI, faceI);
    return v / double(N);
}
vec3 faceNorm(mesh::Interface const &mesh, u32 const cellI, u32 const faceI) noexcept
{
    vec3 v = {0., 0., 0.};
    u32 const N = mesh.pointCount(cellI, faceI);
    vec3 const p0 = mesh.point(cellI, faceI, 0u);
    for(u32 pointI = 1u; pointI + 1u < N; ++pointI)
    {
        vec3 const p1 = mesh.point(cellI, faceI, pointI     );
        vec3 const p2 = mesh.point(cellI, faceI, pointI + 1u);
        v = v + cross(p1 - p0, p2 - p0);
    }
    return v * 0.5;
}
double cellVolume(mesh::Interface const &mesh, u32 const cellI) noexcept
{
    double v = 0.;
    vec3 const c = cellCenter(mesh, cellI);
    u32 const N = mesh.faceCount(cellI);
    for(u32 faceI = 0u; faceI < N; ++faceI)
    {
        vec3 const f = faceCenter(mesh, cellI, faceI);
        vec3 const n = faceNorm  (mesh, cellI, faceI);
        v += std::abs(dot(n, f - c));
    }
    return v / 3.;
}
