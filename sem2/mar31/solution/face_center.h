vec3 faceCenter(mesh::Interface const &mesh, u32 const cellI, u32 const faceI) noexcept
{
    vec3 v = {0., 0., 0.};
    u32 const N = mesh.pointCount(cellI, faceI);
    for(u32 pointI = 0u; pointI < N; ++pointI)
        v = v + mesh.point(cellI, faceI, pointI);
    return v / double(N);
}
