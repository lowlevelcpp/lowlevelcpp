#pragma once
#include <cstdint>
#include "vec3.h"
namespace mesh
{

/*
 * mesh = array of cells
 * cell = array of faces
 * face = array of points
 * every array has natural indexing from 0 to array.size() - 1
 */

using u32 = std::uint32_t;

struct Interface
{
    virtual vec3 point       (u32 cellI, u32 faceI, u32 pointI) const noexcept = 0;
    virtual u32  pointCount  (u32 cellI, u32 faceI)             const noexcept = 0;
    virtual u32   faceCount  (u32 cellI)                        const noexcept = 0;
    virtual u32   cellCount  ()                                 const noexcept = 0;

    virtual ~Interface() {}
};

} // namespace mesh
