#include <cstdint>
#include "../mesh.h"

using u32 = mesh::u32;
namespace student
{
#include "../solution/cube_mesh.h"
} // namespace student

#include "../solution/cell_volume.h"

#include <iostream>
int main()
{
    student::CubeMesh const c;
    bool good = true;
    for(u32 i = 0u; i < c.cellCount(); ++i)
    {
        good &= 1. == cellVolume(c, i);
        for(u32 j = 0u; j < c.faceCount(i); ++j)
            good &= 1. == dot(faceNorm(c, i, j), faceNorm(c, i, j));
    }
    std::cout << good << std::endl;
}
