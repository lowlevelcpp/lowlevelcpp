#include <cstdint>
#include "../mesh.h"

using u32 = mesh::u32;
namespace student
{
#include "../solution/cell_volume.h"
} // namespace student

#include "../cylinder.h"
#include <iostream>
int main()
{
    mesh::Cylinder const c = {10u, 10u, 10u};
    for(u32 i = 0u; i < c.cellCount(); ++i)
    {
        double const v = student::cellVolume(c, i);
        std::cout << v << ' ';
    }
}
