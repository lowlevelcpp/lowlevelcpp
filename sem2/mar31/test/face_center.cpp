#include <cstdint>
#include "../mesh.h"

using u32 = mesh::u32;
namespace student
{
#include "../solution/face_center.h"
} // namespace student

#include "../cylinder.h"

#include <iostream>
int main()
{
    mesh::Cylinder const c = {30u, 20u, 10u};
    for(u32 i = 100u; i < 110u; ++i)
    for(u32 j =   0u; j < c.faceCount(i); ++j)
    {
        vec3 const v = student::faceCenter(c, i, j);
        std::cout << v.x << ' ' << v.y << ' ' << v.z << std::endl;
    }
}
