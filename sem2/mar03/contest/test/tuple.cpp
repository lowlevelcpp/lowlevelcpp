namespace student
{

template<typename T>
struct tuple_wrap
{
    T value;
};
template<typename... Ts>
struct tuple : tuple_wrap<Ts>... {};

#include "../solution/tuple.h"

} // namespace student

#include <iostream>
int main()
{
    student::tuple<int, float, char> tup0;
    student::get<int  >(tup0) = 0;
    student::get<float>(tup0) = 1.f;
    student::get<char >(tup0) = '2';
    std::cout << student::get<int  >(tup0) << ' ';
    std::cout << student::get<float>(tup0) << ' ';
    std::cout << student::get<char >(tup0) << std::endl;
    student::get<int  >(tup0) = 1;
    student::get<float>(tup0) = 2.f;
    student::get<char >(tup0) = '3';
    student::tuple<int, float, char> const tup = tup0;
    std::cout << student::get<int  >(tup) << ' ';
    std::cout << student::get<float>(tup) << ' ';
    std::cout << student::get<char >(tup) << std::endl;
    
}
