namespace student
{
    
template<typename...>
struct Function;
template<typename R, typename... Args>
struct Function<R(Args...)>
{
    virtual ~Function() {};
    virtual R operator()(Args...) const = 0;
};

#include "../solution/function.h"

} // namespace student

#include <vector>
void sort(int *begin, int *end, student::Function<bool(int, int)> const &compare)
{
    for(int *i = begin; i < end; ++i)
    for(int *j = i + 1; j < end; ++j)
        if(compare(*j, *i))
            std::iter_swap(i, j);
}

#include <iostream>
int main()
{
    std::vector<int> v;
    
    int x;
    while(std::cin >> x)
        v.push_back(x);
        
    sort(&v[0], &v[v.size()], student::Less{});
    for(int i : v)
        std::cout << i << ' ';
    std::cout << std::endl;
    sort(&v[0], &v[v.size()], student::Greater{});
    for(int i : v)
        std::cout << i << ' ';
    std::cout << std::endl;
}
