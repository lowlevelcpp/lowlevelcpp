#include <vector>

namespace student
{

template<typename T>
struct stack : private std::vector<T>
{
    T       &top();
    T const &top() const;

    bool        empty() const;
    std::size_t  size() const;

    void push(T const &value);
    void push(T      &&value);

    void pop();

    template<typename... Args>
    void emplace(Args &&... args)
    {
        this->emplace_back(static_cast<Args &&>(args)...);
    }
};

#include "../solution/stack.h"

} // namespace student

#include <iostream>
struct Test
{
    int number;

    ~Test()
    {
        std::cout << "Test::~Test(): " << number << std::endl;
    }
    Test()
    {
        std::cout << "Test::Test()" << std::endl;
    }
    Test(int const n)
        : number(n)
    {
        std::cout << "Test::Test(int): " << number << std::endl;
    }
    Test(Test const &other)
        : number(other.number)
    {
        std::cout << "Test::Test(Test const &) : " << number << std::endl;
    }
    Test(Test      &&other)
        : number(other.number)
    {
        other.number = 0;
        std::cout << "Test::Test(Test &&) : " << number << std::endl;
    }
    Test &operator=(Test const &other)
    {
        std::cout << "Test &Test::operator=(Test const &) : " << number << " -> " << other.number << std::endl;
        if(this != &other)
        {
            this->~Test();
            new (this) Test(other);
        }
        return *this;
    }
    Test &operator=(Test &&other)
    {
        std::cout << "Test &Test::operator=(Test &&) : " << number << " -> " << other.number << std::endl;
        if(this != &other)
        {
            this->~Test();
            new (this) Test(static_cast<Test &&>(other));
        }
        return *this;
    }
};

int main()
{
    student::stack<Test> s;
    std::cout << s.empty() << std::endl;
    std::cout << s.size() << std::endl;
    
    Test test(-2);
    s.push(test);
    std::cout << s.top().number << std::endl;
    
    for(int i = 0; i < 5; ++i)
        s.push(Test(i + 1));
        
    test.number = -1;
    s.push(std::move(test));
    
    student::stack<Test> copy(s);
    std::cout << copy.empty() << std::endl;
    std::cout << copy.size() << std::endl;
    student::stack<Test> moved(std::move(s));
    
    while(!moved.empty())
    {
        std::cout << moved.top().number << std::endl;
        moved.pop();
    }
}
