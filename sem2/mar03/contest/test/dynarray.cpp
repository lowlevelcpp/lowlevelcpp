#include <cstdlib>

namespace student
{
template<typename T>
class dynarray
{
    T *ptr;
    std::size_t size, capacity;

    void reallocate(std::size_t n);

public:
    ~dynarray()
    {
        for(std::size_t i = 0; i < size; ++i)
            (ptr + i)->~T();
        std::free(ptr);
    }
    dynarray()
        : ptr(nullptr)
        , size(0)
        , capacity(0)
    {}
    dynarray(dynarray<T> const &);
    dynarray(dynarray<T>      &&);
    dynarray<T> &operator=(dynarray<T> const &);
    dynarray<T> &operator=(dynarray<T>      &&);

    T       *begin()       {return ptr;}
    T       *  end()       {return ptr + size;}
    T const *begin() const {return ptr;}
    T const *  end() const {return ptr + size;}

    template<typename... Args>
    void emplace_back(Args &&... args)
    {
        if(size == capacity)
            reallocate(capacity == 0 ? 1 : 2 * capacity);
        new (ptr + size++) T(static_cast<Args &&>(args)...);
    }
};

#include "../solution/dynarray.h"

} // namespace student

#include "../../test.h"

#include <memory>
int main()
{
    student::dynarray<Test> array;
    
    Test test(-2);
    array.emplace_back(test);
    test.number = -1;
    array.emplace_back(std::move(test));
    
    for(int i = 1; i < 5; ++i)
        array.emplace_back(i);
    array.emplace_back(Test(10));
    
    for(Test const &test : array)
        std::cout << test.number << ' ';
    std::cout << std::endl;

    student::dynarray<std::unique_ptr<int>> a;
    a.emplace_back(new int(1));
    a.emplace_back(new int(2));
    a.emplace_back(new int(3));
}
