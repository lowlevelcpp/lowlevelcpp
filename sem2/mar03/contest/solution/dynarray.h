template<typename T>
void dynarray<T>::reallocate(std::size_t n)
{
    if(n <= capacity)
        return;
    T * const p = static_cast<T *>(std::malloc(n * sizeof(T)));
    for(std::size_t i = 0; i < size; ++i)
    {
        new (p + i) T(static_cast<T &&>(ptr[i]));
        (ptr + i)->~T();
    }
    std::free(ptr);
    ptr = p;
    capacity = n;
}
