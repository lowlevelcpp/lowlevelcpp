template<typename T>
T       &stack<T>::top()       {return this->back();}
template<typename T>
T const &stack<T>::top() const {return this->back();}

template<typename T>
bool stack<T>::empty() const {return static_cast<std::vector<T> const *>(this)->empty();}
template<typename T>
std::size_t stack<T>::size() const {return static_cast<std::vector<T> const *>(this)->size();}

template<typename T>
void stack<T>::push(T const &value) {return this->push_back(value);}
template<typename T>
void stack<T>::push(T      &&value) {return this->push_back(static_cast<T &&>(value));}

template<typename T>
void stack<T>::pop() {return this->pop_back();}
