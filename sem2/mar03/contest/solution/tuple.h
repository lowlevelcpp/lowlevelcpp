template<typename T, typename... Ts>
T       &get(tuple<Ts...>       &tup)
{
    return static_cast<tuple_wrap<T>       &>(tup).value;
}
template<typename T, typename... Ts>
T const &get(tuple<Ts...> const &tup)
{
    return static_cast<tuple_wrap<T> const &>(tup).value;
}
