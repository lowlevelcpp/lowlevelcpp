struct Less : Function<bool(int, int)>
{
    bool operator()(int a, int b) const override {return a < b;}
};
struct Greater : Function<bool(int, int)>
{
    bool operator()(int a, int b) const override {return a > b;}
};
