#include "test.h"
int main()
{
    //Test *ptr = new Test[4];
    //delete[] ptr;
    Test *ptr = static_cast<Test *>
    (
        std::malloc(sizeof(Test) * 10)
    );
    new (ptr) Test(0);
    new (ptr + 1) Test(1);
    (ptr + 1)->~Test();
    ptr->~Test();
}
