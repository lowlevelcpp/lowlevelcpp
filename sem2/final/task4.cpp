#include <algorithm>
#include <numeric>

unsigned int aliveKwama = 0u;
class Kwama : public Creature
{
protected:
    unsigned int age;
public:
    virtual ~Kwama() = default;
    Kwama(double a, double b, double c, double d, unsigned int e)
        : Creature(a, b, c, d)
        , age(e)
    {
        ++aliveKwama;
    }

    virtual unsigned int harvest()
    {
        setAlive(false);
        --aliveKwama;
        return getPrice();
    }

    virtual void actLikeAKwama() = 0;
    virtual unsigned int getPrice() const = 0;
};

class Queen : public Kwama
{
    unsigned int cuttle, eggs;
    std::vector<Kwama *> mine;
public:
    ~Queen() override
    {
        for(Kwama *kwama : mine)
            delete kwama;
    }
    Queen(unsigned int age, unsigned int eggs)
        : Kwama(0, 0, 0, 0, age)
        , cuttle(1)
        , eggs(eggs)
    {}
    virtual void actLikeAKwama() override
    {
        cout << "HHHHFFFFHHH" << endl;
        cuttle += 10;
        age++;
    }
    virtual unsigned int getPrice() const override
    {
        return cuttle * 109 + eggs * 156;
    }
    void addUnit(Kwama *kwama)
    {
        mine.push_back(kwama);
    }
    void manageMine()
    {
        actLikeAKwama();
        for(Kwama *kwama : mine)
            if(kwama->isAlive())
                kwama->actLikeAKwama();
    }
    unsigned int getTotalPrice() const
    {
        auto const op = [](unsigned int accum, Kwama *kwama)
        {
            return accum + (kwama->isAlive() ? kwama->getPrice() : 0u);
        };
        return std::accumulate(mine.begin(), mine.end(), getPrice(), op);
    }
    unsigned int getTotalUnits() const
    {
        return 1u + mine.size();
    }
    unsigned int getTotalUnitsAlive() const
    {
        auto const op = [](unsigned int accum, Kwama *kwama)
        {
            return accum + (kwama->isAlive() ? 1u : 0u);
        };
        return std::accumulate(mine.begin(), mine.end(), 1u, op);
    }
    unsigned int harvestExpensive()
    {
        if(mine.size() == 0u)
            return 0u;

        auto const less = [](Kwama *left, Kwama *right)
        {
            if(!right->isAlive())
                return false;
            if(!left->isAlive())
                return true;
            else
                return left->getPrice() < right->getPrice();
        };
        auto const candidate = *std::max_element(mine.begin(), mine.end(), less);
        return candidate->isAlive()
            ? candidate->harvest()
            : 0u;
    }
    static unsigned int getNKwamasAlive() {return aliveKwama;}
};
