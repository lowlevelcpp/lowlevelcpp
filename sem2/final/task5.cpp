float dist2(vec4 const &p, AABB const &box)
{
    return dist2(p, min(max(p, box.min), box.max));
}
vec4 nearestFrom(vec4 const &p, Node const * const tree)
{
    Node::Kids const * const kids = tree->pKids();
    if(nullptr == kids)
        return *tree->pPoint();
    else
    {
        float const dLeft  = dist2(p, kids-> left->box());
        float const dRight = dist2(p, kids->right->box());

        Node const * const closer  = dLeft < dRight ? kids-> left : kids->right;
        Node const * const further = dLeft < dRight ? kids->right : kids-> left;
        float        const    dFar = dLeft < dRight ? dRight : dLeft;

        vec4 const pc = nearestFrom(p, closer);
        if(dist2(p, pc) < dFar)
            return pc;

        vec4 const pf = nearestFrom(p, further);
        return dist2(p, pc) < dist2(p, pf) ? pc : pf;
    }
}
