class AABB
{
    Point lb, rt;

public:
    AABB(const Point& a, const Point& b)
        : lb(a)
        , rt(b)
    {}

    bool correct() const
    {
        return lb.getX() <= rt.getX()
            && lb.getY() <= rt.getY();
    }
    double diameter() const
    {
        return (rt - lb).dist();
    }
    bool contains(const Point& p) const
    {
        return AABB(lb, p).correct()
            && AABB(p, rt).correct();
    }
    bool contains(const AABB& ab) const
    {
        return contains(ab.lb)
            && contains(ab.rt);
    }
};
