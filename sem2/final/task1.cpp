#include <algorithm>
#include <iostream>
#include <map>
#include <string>

unsigned int complexity(std::string const &s)
{
    unsigned int maxvalue = 0u;
    unsigned int value = 0u;
    char const * const begin = "lightdark";
    char const * const end = begin + 9;
    for(char const c : s)
    {
        if(c == ' ')
        {
            maxvalue = std::max(maxvalue, value);
            value = 0u;
        }
        else if(std::find(begin, end, c) != end)
            value += 1u;
    }
    return std::max(maxvalue, value);
}

int main()
{
    std::map<unsigned int, std::string> sentence;

    std::string line;
    std::getline(std::cin, line);
    while(std::getline(std::cin, line))
    {
        line.pop_back();
        unsigned int const c = complexity(line);
        if(sentence.find(c) == sentence.end())
            sentence[c] = line;
    }

    std::cout << sentence.rbegin()->second << std::endl;
}
