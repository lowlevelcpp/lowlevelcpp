#include <cstdint>

using u64 = std::uint64_t;
using f64 = double;
struct Record
{
    u64 recordID;
    u64 mimoidID;
    u64 timestamp;
    f64 length;
    f64 width;
    f64 height_max;
    f64 height_avg;
    u64 mounds;
    u64 color;
    u64 age;
};

#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>
#include <map>
int main()
{
    std::map<u64, std::map<u64, Record>> mimoid;

    {
        Record r;
        std::string line;
        while(std::getline(std::cin, line))
        {
            std::istringstream iss(line);
            if((iss >> r.recordID
                    >> r.mimoidID
                    >> r.timestamp
                    >> r.length
                    >> r.width
                    >> r.height_max
                    >> r.height_avg
                    >> r.mounds
                    >> std::hex >> r.color >> std::dec
                    >> r.age) && r.color <= 0xffffffu
            )
                mimoid[r.mimoidID][r.timestamp] = r;
        }
    }
    std::map<u64, Record> filtered;

    for(auto const &pair : mimoid)
    {
        auto const begin = pair.second.begin();

        u64 const t0 = begin->first;
        Record const &r0 = begin->second;

        f64 length = r0.length;
        f64 width  = r0.width;
        for(auto const &p : pair.second)
        {
            u64 const t = p.first;
            Record const &r = p.second;
            if(r.age - t == r0.age - t0
                && length >= r.length
                && width  >= r.width)
            {
                length = r.length;
                width  = r.width;
                filtered[r.timestamp] = r;
            }
        }
    }

    for(auto const &pair : filtered)
        std::cout << pair.second.recordID << ' ';
}
