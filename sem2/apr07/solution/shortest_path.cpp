#include <iostream>
#include <map>
#include <vector>

#include <stack>

#include <cstdint>

using u64 = std::uint64_t;
int main()
{
    std::map<u64, std::vector<std::pair<u64, u64>>> graph;

    u64 a, b, c;
    while(std::cin >> a >> b >> c)
    {
        graph[a].emplace_back(b, c);
        graph[b].emplace_back(a, c);
    }

    std::vector<u64> pathLength(graph.size(), u64(-1));
    std::stack<u64> s;

    s.push(0);
    pathLength[0] = 0;

    while(!s.empty())
    {
        u64 const i = s.top();
        s.pop();

        for(auto const &pair : graph[i])
        {
            u64 const j = pair.first;
            u64 const w = pair.second;
            if(pathLength[i] + w < pathLength[j])
            {
                pathLength[j] = pathLength[i] + w;
                s.push(j);
            }
        }
    }
    for(u64 const l : pathLength)
        std::cout << l << ' ';
    std::cout << std::endl;
}
