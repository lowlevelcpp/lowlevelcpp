#include <algorithm>
std::vector<int> kSmallest(std::vector<int> v, std::size_t k)
{
    std::nth_element(v.begin(), v.begin() + k, v.end());
    return {v.begin(), v.begin() + k};
}
