template<typename...>
struct function;
template<typename R, typename... Args>
class function<R(Args...)>
{
    struct base
    {
        virtual R operator()(Args &&...) const noexcept = 0;
        virtual ~base() = default;
    };

    template<typename F>
    struct impl : base
    {
        F func;

        impl(F const &f) : func(f) {}
        impl(F      &&f) : func(static_cast<F &&>(f)) {}

        R operator()(Args &&...args) const noexcept override
        {
            return func(static_cast<Args &&>(args)...);
        }
    };
    std::unique_ptr<base> ptr;

public:
    template<typename F>
    function(F f) noexcept
        : ptr(new impl<F>(static_cast<F &&>(f)))
    {}
    R operator()(Args ... args) const noexcept
    {
        return ptr->operator()(static_cast<Args &&>(args)...);
    }
};
