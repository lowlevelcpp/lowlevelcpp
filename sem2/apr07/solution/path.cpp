#include <iostream>
#include <map>
#include <vector>

#include <stack>

#include <cstdint>

using u64 = std::uint64_t;
int main()
{
    std::map<u64, std::vector<u64>> graph;

    u64 a, b;
    while(std::cin >> a >> b)
    {
        graph[a].push_back(b);
        graph[b].push_back(a);
    }

    std::stack<u64> s;

    std::vector<bool> visited(graph.size(), false);

    s.push(0);
    visited[0] = true;

    while(!s.empty())
    {
        u64 const i = s.top();
        s.pop();

        for(u64 const j : graph[i])
        {
            if(j + 1 == graph.size())
            {
                std::cout << "true" << std::endl;
                return 0;
            }
            if(!visited[j])
            {
                s.push(j);
                visited[j] = true;
            }
        }
    }
    std::cout << "false" << std::endl;
}
