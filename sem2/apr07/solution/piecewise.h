#include <algorithm>
double PiecewiseLinearFunction::operator()(double const x) const
{
    auto const cmp = +[](Point const &p1, Point const &p2)
    {
        return p1.first < p2.first;
    };
    auto const it = std::lower_bound(data.begin(), data.end(), Point{x, 0.}, cmp);
    auto const i = it - data.begin() - 1;

    if(i < 0)
        return data.front().second;
    if(std::size_t(i + 1) >= data.size())
        return data. back().second;

    std::size_t const i0 = std::size_t(i);
    std::size_t const i1 = i0 + 1u;

    double const x0 = data[i0].first;
    double const x1 = data[i1].first;
    double const y0 = data[i0].second;
    double const y1 = data[i1].second;

    double const t = (x - x0) / (x1 - x0);
    return (1. - t) * y0 + t * y1;
}
