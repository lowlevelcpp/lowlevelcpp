Кусочно-линейная функция (f : double -> double) определяется массивом пар (x: double, y: double), отсортированных по координате x -- обозначим его как [(x_i, y_i)], а его размер как n. Никакие две x-координаты точек не совпадают.

Тогда, f(x) можно вычислить, если определить i, при котором x_i <= x <= x_{i + 1}:

t = (x - x_i) / (x_{i + 1} - x_i),
f(x) = (1 - t) * y_i + t * y_{i + 1}.
Если же такого i не нашлось, то либо x < x_0, либо x > x_{n - 1}; в первом случае, f(x) = y_0, во втором, f(x) = y_{n - 1}.

Соответственно, такую функцию можно описать вот таким классом:

class PiecewiseLinearFunction
{
    using Point = std::pair<double, double>;
    std::vector<Point> data;
public:
    template<typename It>
    PiecewiseLinearFunction(It begin, It end) noexcept
        : data(begin, end)
    {}
    double operator()(double const x) const;
};
Допишите реализацию метода operator(), который должен вычислять значение функции в точке x.

Пример того, как можно собрать функцию:

std::pair<double, double> a[] =
{
    {0., 1.},
    {1., 2.},
    {3., 6.},
};
PiecewiseLinearFunction const f = {a, a + 3};
#include-ить можно всё, что угодно.
