#include <memory>

namespace student
{
#include "../solution/function.h"
}

#include "../test.h"
#include <iostream>
int main()
{
    student::function<int(Test)> f = [x = Test(1)](Test y) {return x.number + y.number;};
    std::cout << f(Test(3)) << std::endl; // 4
}
