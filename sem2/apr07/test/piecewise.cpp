#include <vector>
class PiecewiseLinearFunction
{
    using Point = std::pair<double, double>;
    std::vector<Point> data;
public:
    template<typename It>
    PiecewiseLinearFunction(It begin, It end) noexcept
        : data(begin, end)
    {}
    double operator()(double const x) const;
};

#include "../solution/piecewise.h"

#include <iostream>
int main()
{
    std::pair<double, double> a[] =
    {
        {0., 1.},
        {1., 2.},
        {3., 6.},
    };
    PiecewiseLinearFunction const f = {a, a + 3};

    double x0 = -1.;
    double x1 =  4.;
    unsigned int const n = 101u;
    for(unsigned int i = 0u; i < n; ++i)
    {
        double const x = x0 + i * (x1 - x0) / (n - 1);
        std::cout << x << ' ' << f(x) << std::endl;
    }
}
