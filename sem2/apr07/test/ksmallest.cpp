#include <vector>
#include "../solution/ksmallest.h"

std::vector<int> solution(std::vector<int> v, std::size_t k)
{
    std::nth_element(v.begin(), v.begin() + k, v.end());
    return {v.begin(), v.begin() + k};
}

#include <iostream>
#include <random>
#include <algorithm>
#include <chrono>
int main()
{
    unsigned int N, k;
    std::cin >> N >> k;

    std::vector<int> v(N);
    
    std::mt19937 gen;
    std::uniform_int_distribution<int> distr(0, 1 << 24);
    for(auto i = 0u; i < N; ++i)
        v[i] = distr(gen);
    
    auto const now0 = std::chrono::steady_clock::now();
    auto const vk = kSmallest(v, k);
    auto const now1 = std::chrono::steady_clock::now();
    auto const vs = solution(v, k);
    auto const now2 = std::chrono::steady_clock::now();
    auto const dt10 = (now1 - now0).count();
    auto const dt21 = (now2 - now1).count();
    bool const good = (dt10 < dt21 * 5)
      && std::is_permutation(vk.begin(), vk.end(), vs.begin());
    std::cout << (good ? "OK" : "FAIL" ) << std::endl;
}
