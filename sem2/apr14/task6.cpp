#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <set>
#include <cctype>
#include <algorithm>

class ResultsHolder {
    std::map<std::string, unsigned int> table;
public:
    /**
     * Добавить в общую таблицу результат экзамена студента. 
     * Параметры:
     * - full_name - строка с полным именем студента
     * - mark - оценка (от 1 до 10)
     * (Гарантируется, что совпадений полных имён у разных студентов не бывает.)
     * 
     * Если студента с данным именем нет в результатах - добавить его.
     * Если студент с таким именем есть - обновить его результат 
     * (потому что апелляции вполне возможны).
     */
    void update(const std::string& full_name, unsigned mark)
    {
        table[full_name] = mark;    
    }
    /**
     * Вывести студентов, отсортировав по именам по алфавиту.
     *
     * Вывод в std::cout.
     *
     * Формат вывода:
     * Alex 7
     * Anastasia 9
     * Anny 5
     * Ivan 10
     * Nikita 8
     */
    void print_students() const
    {
        for(auto const &pair : table)
            std::cout << pair.first << " " <<  pair.second << std::endl;
    }
    /**
     * Вывести студентов, отсортировав сперва по оценкам по убыванию,
     * а при равных оценках - по именам по алфавиту.
     *
     * Вывод в std::cout.
     *
     * Формат вывода:
     * Ivan 10
     * Anastasia 9
     * Nikita 8
     * Alex 7
     * Anny 5
     */
    void print_standings() const
    {
        using Pair = std::pair<std::string, unsigned int>;
        std::vector<Pair> v(table.begin(), table.end());
        auto const comp = [](Pair const &left, Pair const &right)
        {
            return left.second > right.second;
        };

        std::stable_sort(v.begin(), v.end(), comp);
        for(auto const &pair : v)
            std::cout << pair.first << " " <<  pair.second << std::endl;
    }

    /**
     * Обработать запрос военкомата.
     *
     * Военкомат передаёт список имён и хочет призвать этих людей в армию.
     * Если у этих людей неуд-ы (оценка ниже 3), то:
     * - вернуть эти имена в ответе военкомату;
     * - удалить этих людей из общей таблицы (потому что теперь они не студенты).
     *
     * Входной параметр: сет строк с именами, кого хочет призвать военкомат.
     * Возвращаемое значение: сет строк с именами, кого действительно можно призвать
     * (или пустой сет, если призвать никого нельзя).
     * Гарантируется, что военкомат не будет пытаться призвать несуществующих студентов.
     */
    std::set<std::string> process_military_request(const std::set<std::string> &names)
    {
        std::set<std::string> luckiests;
        for(std::string const &name : names)
        {
            if (table[name] < 3)
            {
                luckiests.insert(name);
                table.erase(name);
            }
        }
        return luckiests;
    }
};
