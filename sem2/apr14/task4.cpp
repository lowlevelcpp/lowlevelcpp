#include <iostream>
#include <map>
int main()
{
    unsigned int n;
    std::cin >> n;
    std::map<int, int, std::greater<int>> f;
    for(auto i = 0u; i < n; i++)
    {
        int a;
        std::cin >> a;
        if( f.find(a) == f.end())
            f[a] = 1;
        else
            f[a] += 1;
    }
    for(std::pair<int, int> p : f)
    {
        if (p.second == 1)
        {
            std::cout << p.first << std::endl;
            break;
        }   
    }
}
