Point operator-(Point const &p1, Point const &p2)
{
    return Point
    (
        p1.x() - p2.x(),
        p1.y() - p2.y(),
        p1.z() - p2.z()
    );
}
double dot(Point const &p1, Point const &p2)
{
    return p1.x() * p2.x() + p1.y() * p2.y() + p1.z() * p2.z();
}
class Sphere
{
    Point center;
    double r;
public:
    Sphere(Point const &p, double const R)
        : center(p)
        , r(R)
    {}
    
    // Проверка, попадает ли заданная точка p в данную сферу.
    // (Расстояния сравнивать с точностью 1e-6.)
    bool covers(const Point& p) const
    {
        Point const a = p - center;
        return dot(a, a) < r * r;
    }
};
