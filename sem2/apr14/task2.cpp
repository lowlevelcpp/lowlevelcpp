class MoneyBox 
{
    unsigned int value = 0u;
    unsigned int count = 0u;

public:
    // Конструктор и деструктор, если нужны

    // Добавить монетку достоинством value
    void addCoin(unsigned int v)
    {
        value += v;
        count += 1;
    }

    // Получить текущее количество монеток в копилке
    unsigned int getCoinsNumber() const
    {
        return count;
    }

    // Получить текущее общее достоинство всех монеток
    unsigned int getCoinsValue() const
    {
        return value;
    }
};
