#include <exception>
class Ball {
    enum State
    {
        Intact = 0,
        Destroyed = 1,
        Lost = 2,
    } state = Intact;

public:
    // Конструктор и деструктор шара, если нужны

    // Попытка сломать шар.
    // Для первого целого шара должна заканчиваться успешно.
    // Если один шар уже был сломан, выбросить std::exception
    // Если пытаемся ломать уже сломанный или потерянный шар, выбросить std::exception
    void destroy()
    {
        static bool destroyed = false;
        if(destroyed || state == Lost)
            throw std::exception();
        else
        {
            destroyed = true;
            state = Destroyed;
        }
    }

    // Попытка потерять шар.
    // Для первого целого шара должна заканчиваться успешно.
    // Если один шар уже был потерян, то выбросить std::exception
    // Если пытаемся терять уже сломанный или потерянный шар, выбросить std::exception
    void lose()
    {
        static bool lost = false;
        if(lost || state == Destroyed)
            throw std::exception();
        else
        {
            lost = true;
            state = Lost;
        }
    }
};
