#include <exception>
class GasStation {
    unsigned int capacity;
    unsigned int amount = 0u;
public:
    // Конструктор, принимающий один параметр - ёмкость резервуара колонки
    // Резервуар создаётся пустой
    GasStation(unsigned int n)
        : capacity(n)
    {}
    // Залить в резервуар колонки n литров топлива
    // Если столько не влезает в резервуар - не заливать ничего, выбросить std::exception
    void fill(unsigned int n)
    {
        if (amount + n <= capacity)
            amount = amount + n;
        else
            throw std::exception();
    }

    // Заправиться, забрав при этом из резервура n литров топлива
    // Если столько нет в резервуаре - не забирать из резервуара ничего, выбросить std::exception
    void tank(unsigned int n)
    {
        if (amount >= n)
            amount -= n;
        else
            throw std::exception();
    }

    // Запросить остаток топлива в резервуаре
    unsigned int get_limit() const
    {
        return amount;
    }
};
