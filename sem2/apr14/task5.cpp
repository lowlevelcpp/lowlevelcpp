#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <set>
#include <cctype>
int main()
{
    std::map<std::string, std::set<std::string> > f;
    unsigned int n;
    std::cin >> n;
    for ( unsigned int i = 0; i < n; i++ )
    {
        std::string a;
        std::cin >> a;
        std::string copy = a;
        for(char& c : copy)
        {
            c = std::tolower(c);
        }
        f[copy].insert(a);
    }
    std::map <std::size_t, std::vector<std::string>, std::greater<std::size_t> > ff;
    for(auto i : f)
    {
        auto const s = i.second.size();
        if(s > 2)
            ff[s].push_back(i.first);
    }

    for(auto i : ff)
    {
        for(std::string &s : i.second)
            std::cout << s <<std::endl;
    }
}
