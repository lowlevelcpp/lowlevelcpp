#include "../solution/list.h"
#include "../solution/range.h"

namespace student
{
//#include "../solution/recursive/createList.h"
#include "../solution/iterative/createList.h"
}

#include "../solution/recursive/destroyList.h"

#include <iostream>
int main()
{
    int const a[] = {1, 2, 3, 4, 5};
    List const list = student::createList({a, a + 5});
    for(Node *node = list; node != nullptr; node = node->next)
        std::cout << node->value;
    std::cout << std::endl;
    destroyList(list);
}
