#include "../solution/list.h"

namespace student
{
#include "../solution/cons.h"
}

using student::cons;

#include <iostream>
int main()
{
    Node * const list = cons(1, cons(2, cons(3, nullptr)));

    std::cout << list->value << " ";
    std::cout << list->next->value << " ";
    std::cout << list->next->next->value << std::endl;
    std::free(list->next->next);
    std::free(list->next);
    std::free(list);
}
