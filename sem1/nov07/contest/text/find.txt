Реализуйте функцию

ListIterator find(ListIterator first, ListIterator last, int n);

возвращающую итератор на первый элемент в списке, равный n, либо last, если такого не нашлось.

https://en.cppreference.com/w/cpp/algorithm/find
