#include "../bits/tree.h"

namespace student
{
#include "../solution/insert.h"
}

#include "../solution/create_tree.h"
#include "../solution/destroy_tree.h"

#include "../bits/list.h"
#include "../solution/to_list.h"

#include <iostream>
#include <sstream>
int main()
{
    Tree tree = nullptr;

    std::string line[2];
    std::getline(std::cin, line[0]);
    std::getline(std::cin, line[1]);
    std::istringstream iss0(line[0]);
    std::istringstream iss1(line[1]);
    
    int x;
    while(iss0 >> x)
        tree = student::insert(x, tree);
    while(iss1 >> x)
        tree = student::insert(cons(tree, x, tree), tree);

    List const pre = preorder(tree);
    List const  in =  inorder(tree);
    
    for(Tree const node : pre)
        std::cout << node->value << ' ';
    std::cout << std::endl;
    for(Tree const node : in)
        std::cout << node->value << ' ';
    std::cout << std::endl;
        
    destroyTree(tree);
    destroy(pre);
    destroy(in);
}
