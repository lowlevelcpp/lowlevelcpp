#include "../bits/tree.h"

namespace student
{
#include "../solution/merge.h"
}

#include "../solution/destroy_tree.h"
#include "../solution/insert.h"

#include "../bits/list.h"
#include "../solution/to_list.h"

#include <iostream>
#include <sstream>
int main()
{
    Tree left = nullptr;
    Tree right = nullptr;

    std::string line[2];
    std::getline(std::cin, line[0]);
    std::getline(std::cin, line[1]);
    std::istringstream iss0(line[0]);
    std::istringstream iss1(line[1]);
    
    int x;
    while(iss0 >> x)
        left  = insert(x, left );
    while(iss1 >> x)
        right = insert(x, right);

    Tree const tree = student::merge(left, right);
    List const  in =  inorder(tree);
    
    for(Tree const node : in)
        std::cout << node->value << ' ';
    std::cout << std::endl;
        
    destroyTree(tree);
    destroy(in);
}
