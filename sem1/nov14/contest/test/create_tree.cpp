#include "../bits/tree.h"

namespace student
{
#include "../solution/create_tree.h"
}

#include "../solution/destroy_tree.h"

#include <iostream>
#include <vector>

#include "../bits/print_tree.h"

int main()
{
    std::vector<int> v;
    int x;
    while(std::cin >> x)
        v.push_back(x);

    Tree const myTree = student::createTree(&v[0], &v[v.size()]);
    Tree const emptyTree = student::createTree(nullptr, nullptr);
    std::cout << myTree << std::endl;
    destroyTree(myTree);
    destroyTree(emptyTree);
}
