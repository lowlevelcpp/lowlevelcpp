#include "../bits/tree.h"

namespace student
{
#include "../solution/cons.h"
}

using student::cons;

#include "../solution/destroy_tree.h"
#include "../solution/tree_size.h"

#include <iostream>
int main()
{
    Tree const tree = cons
    (
        cons(nullptr, 1, nullptr),
        2,
        cons
        (
            cons(nullptr, 3, nullptr),
            4,
            nullptr
        )
    );
    std::cout << treeSize(tree) << std::endl;
    std::cout << (tree->value == 2);
    std::cout << (tree->left->value == 1);
    std::cout << (tree->right->value == 4);
    std::cout << (tree->right->left->value == 3);
    std::cout << std::endl;
    destroyTree(tree);
}
