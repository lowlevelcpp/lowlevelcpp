#include "../bits/tree.h"
namespace student
{
#include "../solution/destroy_tree.h"
}

#include "../solution/cons.h"
#include "../solution/tree_size.h"

#include <iostream>
int main()
{
    Tree const myTree = cons
    (
        cons(nullptr, 1, nullptr),
        2,
        cons
        (
            cons(nullptr, 3, nullptr),
            4,
            nullptr
        )
    );
    std::cout << treeSize(myTree) << std::endl;
    student::destroyTree(myTree);
    student::destroyTree(nullptr);
}
