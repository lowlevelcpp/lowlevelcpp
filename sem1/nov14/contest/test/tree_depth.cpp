#include "../bits/tree.h"

namespace student
{
#include "../solution/tree_depth.h"
}

#include "../solution/create_tree.h"
#include "../solution/destroy_tree.h"
#include "../solution/insert.h"

#include <iostream>
int main()
{
    Tree myTree = nullptr;
    int x;
    while(std::cin >> x)
        myTree = insert(x, myTree);

    std::cout << student::treeDepth(myTree) << std::endl;
    destroyTree(myTree);
}
