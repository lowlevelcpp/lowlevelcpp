#include "../bits/tree.h"

namespace student
{
#include "../solution/minmax.h"
}

#include "../solution/create_tree.h"
#include "../solution/destroy_tree.h"

#include <iostream>
#include <vector>
int main()
{
    std::vector<int> v;
    int x;
    while(std::cin >> x)
        v.push_back(x);

    Tree const tree = createTree(&v[0], &v[v.size()]);
    
    std::cout << student::min(tree) << ' ';
    std::cout << student::max(tree) << std::endl;
        
    destroyTree(tree);
}
