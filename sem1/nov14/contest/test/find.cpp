#include "../bits/tree.h"

namespace student
{
#include "../solution/find.h"
}

#include "../solution/create_tree.h"
#include "../solution/destroy_tree.h"

#include <iostream>
#include <vector>
int main()
{
    int n;
    std::cin >> n;
    
    std::vector<int> v;
    int x;
    while(std::cin >> x)
        v.push_back(x);

    Tree const tree = createTree(&v[0], &v[v.size()]);
    Node const * const node = student::find(n, tree);
    
    if(node == nullptr)
        std::cout << "not found" << std::endl;
    else
        std::cout << "found " << n << std::endl;
        
    destroyTree(tree);
}
