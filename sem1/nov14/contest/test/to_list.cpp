#include "../bits/tree.h"
#include "../bits/list.h"

namespace student
{
#include "../solution/to_list.h"
}

#include "../solution/create_tree.h"
#include "../solution/destroy_tree.h"

#include <iostream>
#include <vector>
int main()
{
    std::vector<int> v;
    int x;
    while(std::cin >> x)
        v.push_back(x);

    Tree const tree = createTree(&v[0], &v[v.size()]);
    List const pre = student::preorder(tree);
    List const  in = student:: inorder(tree);
    
    for(Tree const node : pre)
        std::cout << node->value << ' ';
    std::cout << std::endl;
    for(Tree const node : in)
        std::cout << node->value << ' ';
    std::cout << std::endl;
        
    destroyTree(tree);
    destroy(pre);
    destroy(in);
}
