unsigned int treeDepth(Tree const tree)
{
    if(tree == nullptr)
        return 0u;
    unsigned int const left  = treeDepth(tree->left );
    unsigned int const right = treeDepth(tree->right);
    return 1u + (left < right ? right : left);
}
