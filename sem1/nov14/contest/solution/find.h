Node *find(int const n, Tree const tree)
{
    if(tree == nullptr)
        return nullptr;
    if(tree->value < n)
        return find(n, tree->left );
    if(tree->value > n)
        return find(n, tree->right);
    return tree;
}
